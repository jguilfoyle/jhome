//
//  ViewController.swift
//  jHome
//
//  Created by jeff guilfoyle on 3/20/16.
//  Copyright © 2016 jeff guilfoyle. All rights reserved.
//

import UIKit
import Charts


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class waterWeatherViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate {
    

    
    @IBOutlet weak var poolTemp: UILabel!
    
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet weak var windsLabel: UILabel!
    
    @IBOutlet weak var airTempLabel: UILabel!
    @IBOutlet weak var wuConditionsImage: UIImageView!
   
    
    @IBOutlet weak var forecastLabel: UILabel!
    @IBOutlet weak var spaTempLabel: UILabel!
    
    // @IBOutlet weak var tabBar: UITabBarItem!
 
    
    //let host="http://104.188.20.32:8200"
    //let host="http://192.168.1.129:8200"
    let host="http://search-monitor-aecjp76zcrmtqpy37kppuaao5y.us-west-2.es.amazonaws.com"
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
        //      solarChart.data=nil
   
        fetchPoolTempFromElastic()
        fetchSpaStatus()
        //fetchSpaTempFromElastic()
        fetchWeatherUnderground()
        fetchWeatherUndergroundForecast()
    
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
   
        
        //       self.fetchGenerationFromElastic(2000)
        
        //     _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.fetchDemandFromMeter), userInfo: nil, repeats: true)

        _ = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.fetchPoolTempFromElastic), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(self.fetchWeatherUnderground), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 1800, target: self, selector: #selector(self.fetchWeatherUndergroundForecast), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.fetchSpaStatus), userInfo: nil, repeats: true)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    

    
    
  
    
    
    
    var poolData: NSDictionary?
    
    
    func fetchPoolTempFromElastic()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "\(host)/pool-*/_search?sort=timestamp:desc&size=1"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchPoolTempFromElastic: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.poolData = jsonData as? NSDictionary
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        let hitsParent=self.poolData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        let hit = hits[0] as! NSDictionary
                        let hit2=hit["_source"] as! NSDictionary
                        let poolTemp=hit2["pooltemp"] as! NSNumber
                        let setTemp=hit2["settemp"] as! NSNumber
                        
                        self.poolTemp.text = "\(poolTemp)"
                    })
                }
            }
        })
        task.resume()
    }
    
    var spaData: NSDictionary?
    ///spa/api/v1.0/status
    func fetchSpaStatus()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://spa2.local:5000/spa/api/v1.0/status"
        
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchSpaStatus: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        let spaJsonData = jsonData as? NSDictionary
                        let spaStatus = spaJsonData?["status"] as? NSDictionary
                        self.spaData = spaStatus
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        //let hitsParent=self.spaData!["hits"]! as! NSDictionary
                        //let hits=hitsParent["hits"] as! NSArray
                        //let hit = hits[0] as! NSDictionary
                        //let hit2=hit["_source"] as! NSDictionary
                        //let spaTemp=hit2["temperature"] as! NSNumber
                        // let setTemp=hit2["settemp"] as! NSNumber
                        let spaTemp = self.spaData?["probetemp"] as! String
                        let spaTempNumber = Double(spaTemp)
                        let spaTempString = String(format: "%.1f", arguments: [spaTempNumber!])
                        self.spaTempLabel.text = "\(spaTempString)"
                    })
                }
            }
        })
        task.resume()
    }
    
    
    
    func fetchSpaTempFromElastic()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "\(host)/spa-*/_search?sort=timestamp:desc&size=1"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchSpaTempFromElastic: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.spaData = jsonData as? NSDictionary
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        let hitsParent=self.spaData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        let hit = hits[0] as! NSDictionary
                        let hit2=hit["_source"] as! NSDictionary
                        let spaTemp=hit2["temperature"] as! NSNumber
                        // let setTemp=hit2["settemp"] as! NSNumber
                        
                        self.spaTempLabel.text = "\(spaTemp)"
                    })
                }
            }
        })
        task.resume()
    }
    
    
    // wunderground key
    //   efd234ca5e26b934
    
    
    
    var weatherData: NSDictionary?
    
    func fetchWeatherUnderground()
    {
        NSLog("fetching weather")
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://api.wunderground.com/api/efd234ca5e26b934/conditions/q/92117.json"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchWeatherUnderground: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                        self.weatherData = jsonData["current_observation"] as? NSDictionary
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        if let temp = self.weatherData?["temperature_string"] as? String {
                            self.airTempLabel.text = self.weatherData!["temperature_string"] as? String
                            self.conditionsLabel.text = self.weatherData!["weather"] as? String
                            self.windsLabel.text = self.weatherData!["wind_string"] as? String
                        }
                    })
                }
            }
        })
        task.resume()
    }
    
    var forecastData: NSArray?
    
    func fetchWeatherUndergroundForecast()
    {
        NSLog("fetching weather")
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://api.wunderground.com/api/efd234ca5e26b934/forecast/q/92117.json"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchWeatherUndergroundForecast: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
                        
                        let forecast=jsonData?["forecast"] as? NSDictionary
                        let txt_forecast=forecast?["txt_forecast"] as! NSDictionary
                        self.forecastData=txt_forecast["forecastday"] as? NSArray
                        
                        // self.forecastData = jsonData["forecast"]!!["txt_forecast"]!!["forecastday"] as? NSArray
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        let forecastDataFirst=self.forecastData?[0] as? NSDictionary
                        if let forecasttext = forecastDataFirst?["fcttext"] as? String {
                            self.forecastLabel.text = forecasttext
                        }
                        
                    })
                }
            }
        })
        task.resume()
    }
    
    
    func fetchImage(_ url: String)
    {
        
        let config = URLSessionConfiguration.ephemeral
        
        let url=URL(string: url)
        var request = URLRequest(url: url!)
        request.setValue("Basic d2ViYXBwOmlBTXdvcnRoeQ==", forHTTPHeaderField: "Authorization")
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchImage: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    //             let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.poolData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    //          var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        let hitsParent=self.poolData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        
                        let hit = hits[0] as? NSDictionary
                        //let hit=hits[0] as! NSDictionary
                        let hit2=hit?["_source"] as! NSDictionary
                        let poolTemp=hit2["pooltemp"] as! NSNumber
                        let setTemp=hit2["settemp"] as! NSNumber
                        
                        self.poolTemp.text = "\(poolTemp)"
                        
                        
                    })
                    
                }
            }
        })
        task.resume()
    }
    
    
    func hideTabBar(){
        // self.tabBar.
    }
    
    
    func refreshData() {
        //   solarChart.data=nil

        fetchPoolTempFromElastic()
        fetchSpaStatus()
        //fetchSpaTempFromElastic()
        fetchWeatherUnderground()
        fetchWeatherUndergroundForecast()
      
    }
    
    
    
    
}
