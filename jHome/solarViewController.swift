//
//  solarViewController.swift
//  jHome
//
//  Created by jeff guilfoyle on 10/7/17.
//  Copyright © 2017 jeff guilfoyle. All rights reserved.
//


import UIKit
import Charts


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class solarViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate {
    
    @IBOutlet weak var solarLabel: UILabel!
    
    @IBOutlet weak var demandLabel: UILabel!

    
    @IBOutlet weak var solarChart: LineChartView!
    
   
    
 
    @IBOutlet weak var sunImage: UIImageView!
    

    

    @IBOutlet weak var dailySolarLabel: UILabel!
    
    
    
    //let host="http://104.188.20.32:8200"
    //let host="http://192.168.1.129:8200"
    let host="http://search-monitor-aecjp76zcrmtqpy37kppuaao5y.us-west-2.es.amazonaws.com"
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        solarChart.backgroundColor = nil
 
        
        if self.colors.count < 1 {
            self.setChartColors()
            
        }
        
        solarChart.data=nil
        self.solarDataPoints = []
        self.solarDataIndex = []
        self.fetchGenerationFromElastic(3000)
        
   //     fetchUsageFromElastic()

        //fetchSpaTempFromElastic()

        fetchGenerationFromSolarEdge()
        
    }
    
    func setChartColors() {
        
        
        let chartColors = [
            [0.000, 0.635, 1.000],
            
            [0.380, 0.847, 0.212],
            [0.973, 0.733, 0.008],
            [1.000, 0.149, 0.000],
            [0.761, 0.282, 0.522],
            [0.373, 0.373, 0.373]
        ]
        
        
        for i in 0..<4 {
            let alpha = 1.0 - Double(i)/4.0
            for colorTrio in chartColors {
                let color = UIColor(red: CGFloat(colorTrio[0]), green: CGFloat(colorTrio[1]), blue: CGFloat(colorTrio[2]), alpha: CGFloat(alpha))
                self.colors.append(color)
            }
        }
    }
    
    var colors: [UIColor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        self.fetchFromMeter("get_instantaneous_demand")
        
        //       self.fetchGenerationFromElastic(2000)
        
        //     _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.fetchDemandFromMeter), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.fetchCurrentSolar), userInfo: nil, repeats: true)
   //     _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.fetchUsageFromElastic), userInfo: nil, repeats: true)
  //      _ = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.fetchPoolTempFromElastic), userInfo: nil, repeats: true)
  //      _ = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(self.fetchWeatherUnderground), userInfo: nil, repeats: true)
  //      _ = Timer.scheduledTimer(timeInterval: 1800, target: self, selector: #selector(self.fetchWeatherUndergroundForecast), userInfo: nil, repeats: true)
  //      _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.fetchSpaStatus), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 900, target: self, selector: #selector(self.fetchGenerationFromSolarEdge), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    var demandData: NSDictionary?
    
    
    func fetchDemandFromMeter() {
        self.fetchFromMeter("get_instantaneous_demand")
        
    }
    
    
    func fetchFromMeter(_ command: String)
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "https://rainforestcloud.com:9445/cgi-bin/post_manager"
        let url=URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
        request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
        request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
        
        let paramString = "<Command>\n<Name>\(command)</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchFromMeter: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    if (dataString?.contains("Error") == true) {
                        NSLog("fetchFromMeter \(dataString!)")
                        self.demandLabel.text = "error"
                        self.demandData = nil
                    }
                    else {
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                            self.demandData = jsonData as? NSDictionary
                            //   self.tableView.reloadData()
                        }
                        catch _ {
                            NSLog("error parsing JSON data")
                        }
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    
                    DispatchQueue.main.async(execute: {
                        if (self.demandData != nil) {
                            if (self.demandData!["InstantaneousDemand"] != nil) {
                                
                                let instantaneousDemand=self.demandData!["InstantaneousDemand"] as? NSDictionary
                                let currentDemandString=instantaneousDemand?["Demand"] as? String
                                
                                
                                
                                //     let currentDemandString=self.demandData!["InstantaneousDemand"]!["Demand"]! as? String
                                let parsedDemandString = currentDemandString?.replacingOccurrences(of: "0x", with: "")
                                //currentDemandString="0002c3"
                                var currentDemand = Int(parsedDemandString!, radix: 16)
                                if (currentDemand > 2147483647) {
                                    currentDemand = currentDemand! - 4294967296
                                }
                                let currentDemandkW = (Float(currentDemand!) / 1000.0)
                                let currentDemandOutput = "\(currentDemandkW) kW"
                                //       NSLog("current demand \(currentDemandkW) kW")
                                
                                self.demandLabel.text = currentDemandOutput
                            }
                            
                        }
                    })
                    
                }
            }
        })
        task.resume()
    }
    
    
    
    
    
    
    
    
    
    
    var solarEdgeData: NSDictionary?
    
    
    func fetchGenerationFromSolarEdge()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "https://monitoringapi.solaredge.com/site/231801/overview?api_key=3HPD58OS5IMPJUSP0AVRIAW29ZEUY19H&format=application/json"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchGenerationFromSolarEdge: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    //  let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.solarEdgeData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    
                    DispatchQueue.main.async(execute: {
                        let overview=self.solarEdgeData!["overview"] as! NSDictionary
                        let lastDayData=overview["lastDayData"] as! NSDictionary
                        let dailyTotal=(lastDayData["energy"] as! Double)/1000
                        
                        // let dailyTotal=(self.solarEdgeData!["overview"]!["lastDayData"]!!["energy"]! as? Double)!/1000
                        
                        
                        
                        //   self.sunImage.alpha = CGFloat((Double(currentPower!)/10.0)+0.2)
                        
                        self.dailySolarLabel.text = String(format: "%.2f kWh", dailyTotal)
                        
                        
                        //    self.solarLabel.text = String(currentPower!)
                        
                    })
                    
                }
            }
        })
        task.resume()
    }
    
    
    var solarDataPoints = [Double]()
    var solarDataIndex = [String]()
    
    
    func setSolarChart() {
        solarChart.backgroundColor = nil
        solarChart.chartDescription?.text = ""
        solarChart.noDataText = "You need to provide data for the chart."
        solarChart.legend.enabled = false
        solarChart.rightAxis.enabled = false
        
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<solarDataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: solarDataPoints[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = LineChartDataSet(values: dataEntries, label: "kW")
        
        
        let chartData=LineChartData(dataSet: chartDataSet)
        
        //let chartData = LineChartData(xVals: solarDataIndex, dataSet: chartDataSet)
        chartDataSet.lineWidth=2
        chartDataSet.circleRadius=0
        chartDataSet.colors=[UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        //chartDataSet.
        solarChart.leftAxis.axisMinimum = -0.001
        //solarChart.rightAxis.
        solarChart.data = chartData
        
    }
    
    
    
  
    
    
    
    
    func fetchCurrentSolar() {
        self.fetchGenerationFromElastic(1)
        
    }
    
    var solarData: NSDictionary?
    
    
    func fetchGenerationFromElastic(_ count: Int)
    {
        
        let config = URLSessionConfiguration.ephemeral
        //    let urlString: String = "http://104.188.20.32:8200/solar/_search?sort=timestamp:desc&size=\(count)"
        let urlString: String = "\(host)/solar-*/_search?sort=timestamp:desc&size=\(count)"
        let url=URL(string: urlString)
        var request = URLRequest(url: url!)
        //request.setValue("Basic d2ViYXBwOmlBTXdvcnRoeQ==", forHTTPHeaderField: "Authorization")
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchgenerationfromelastic: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    //     let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.solarData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        if let hitsParent=self.solarData?["hits"]! as? NSDictionary {
                            let hits=hitsParent["hits"] as! [NSDictionary]
                            
                            for hit in hits.reversed() {
                                //let hit=hits[0] as! NSDictionary
                                let hit2=hit["_source"] as! NSDictionary
                                let generation=hit2["generation"] as! NSDictionary
                                let currentPower=generation["kva"] as! Double
                                // let kw
                                
                                
                                //   let currentPower = self.solarData!["hits"]!["generation"]!!["kva"]! as? Double
                                kw = currentPower/1000
                                self.sunImage.alpha = CGFloat(kw/9.0)
                                //       NSLog("current power \(kw) kW")
                                self.solarDataPoints.append(kw)
                                
                                self.solarDataIndex.append("")
                                
                            }
                            self.solarDataPoints.remove(at: 0)
                            self.solarDataIndex.remove(at: 0)
                            self.setSolarChart()
                            // solarChart.data = self.solarDataPoints
                            self.solarLabel.text = String("\(kw) kW")
                            
                        }
                    })
                    
                }
            }
        })
        task.resume()
    }
    
    
    
    
    
   
    
    
    func hideTabBar(){
        // self.tabBar.
    }
    
    
    func refreshData() {
        solarChart.data=nil
        self.solarDataPoints = []
        self.solarDataIndex = []
        self.fetchGenerationFromElastic(3000)
        
    //    fetchUsageFromElastic()
    //    fetchPoolTempFromElastic()
    //    fetchSpaStatus()
        //fetchSpaTempFromElastic()
    //    fetchWeatherUnderground()
    //    fetchWeatherUndergroundForecast()
        fetchGenerationFromSolarEdge()
    }
    
    
    
    
}
