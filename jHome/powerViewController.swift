//
//  ViewController.swift
//  jHome
//
//  Created by jeff guilfoyle on 3/20/16.
//  Copyright © 2016 jeff guilfoyle. All rights reserved.
//

import UIKit
import Charts


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class powerViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate {
    

    
 //   @IBOutlet weak var solarChart: LineChartView!
    
    @IBOutlet weak var usageChart: PieChartView!
    
 
    
    
    //let host="http://104.188.20.32:8200"
    //let host="http://192.168.1.129:8200"
    let host="http://search-monitor-aecjp76zcrmtqpy37kppuaao5y.us-west-2.es.amazonaws.com"
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  solarChart.backgroundColor = nil
        usageChart.backgroundColor = nil
        
        if self.colors.count < 1 {
            self.setChartColors()
            
        }
        

    //    self.fetchGenerationFromElastic(3000)
        
        fetchUsageFromElastic()
   
        
    }
    
    func setChartColors() {
        
        
        let chartColors = [
            [0.000, 0.635, 1.000],
            
            [0.380, 0.847, 0.212],
            [0.973, 0.733, 0.008],
            [1.000, 0.149, 0.000],
            [0.761, 0.282, 0.522],
            [0.373, 0.373, 0.373]
        ]
        
        
        for i in 0..<4 {
            let alpha = 1.0 - Double(i)/4.0
            for colorTrio in chartColors {
                let color = UIColor(red: CGFloat(colorTrio[0]), green: CGFloat(colorTrio[1]), blue: CGFloat(colorTrio[2]), alpha: CGFloat(alpha))
                self.colors.append(color)
            }
        }
    }
    
    var colors: [UIColor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
   //     self.fetchFromMeter("get_instantaneous_demand")
        
        //       self.fetchGenerationFromElastic(2000)
        
        //     _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.fetchDemandFromMeter), userInfo: nil, repeats: true)

        _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.fetchUsageFromElastic), userInfo: nil, repeats: true)


   //     _ = Timer.scheduledTimer(timeInterval: 900, target: self, selector: #selector(self.fetchGenerationFromSolarEdge), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    
    
    
    
    
 
    
    var usageDataPoints = [Double]()
    var usageDataIndex = [String]()
    
    
    func initUsageChart() {
        usageChart.isOpaque = false
        usageChart.holeColor = nil
        usageChart.backgroundColor = nil
        usageChart.legend.enabled = false
        usageChart.chartDescription?.text = ""
        // usageChart.legend = ""
        //usageChart.t
        usageChart.noDataText = "You need to provide data for the chart."
        let circuitList = ["Lights","Lights2","Kitchen","A","Spa-A",
                           "Spa","AC-Theater-A","AC-Theater","Garage","Spare",
                           "Furnace","Master Bath","Pool-A","Pool","Fan",
                           "Wiring closet","Washer","Pool Bath","Play lights","Play outlets",
                           "F","Garage/Stair lights","MBR Lights","MBR Outlets", "1",
                           "2", "3", "4", "5", "6",
                           "30","31","AC"]
        var circuits: [String] = []
        
        var dataEntries: [ChartDataEntry] = []
        //  var dataIndex: [String] = []
        var index=0
        var circuit=0
        for usageItem  in self.usageDataArray! {
            var watts = usageItem["watts"] as! Double
            //     let circuit = usageItem["circuit"]
            //add pool circuits together
            if (circuit == 13 || circuit == 5 || circuit == 7)  {
                //circuit=circuit+1
                let secondCircuit = self.usageDataArray![circuit-1] as! NSDictionary
                let watts2 = secondCircuit["watts"] as! Double
                
                watts = watts + watts2
                
            }
            
            
            if (circuit != 12 && circuit != 4 && circuit != 6 && watts > 0) {
                
                
                let dataEntry = PieChartDataEntry(value: watts, label: circuitList[circuit])
                //  x: watts, y: Double(index))
                circuits.append(circuitList[circuit])
                index=index+1
                dataEntries.append(dataEntry)
            }
            circuit=circuit+1
        }
        
        
        
        let chartDataSet = PieChartDataSet(values: dataEntries, label: "watts")
        chartDataSet.colors = self.colors
        
        
        let chartData = PieChartData(dataSet: chartDataSet)
        
        //  let chartData = PieChartData(-k w: circuits, dataSet: chartDataSet)
        
        
        //  chartDataSet.lineWidth=2
        //  chartDataSet.circleRadius=0
        //  chartDataSet.colors=[UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        //chartDataSet.
        //soChart.leftAxis.startAtZeroEnabled = true
        //solarChart.rightAxis.
        
        usageChart.data = chartData
        
    }
    
    
    
    
    

    
    
    var usageData: NSDictionary?
    var usageDataArray: [NSDictionary]?
    
    // get Ted Spyder usage per circuit
    // http://192.168.1.129:9200/usage/_search?sort=timestamp:desc&size=1
    func fetchUsageFromElastic()
    {
        
        let config = URLSessionConfiguration.ephemeral
        //    let urlString: String = "http://104.188.20.32:8200/solar/_search?sort=timestamp:desc&size=\(count)"
        let urlString: String = "\(host)/usage-*/_search?sort=timestamp:desc&size=1"
        let url=URL(string: urlString)
        var request = URLRequest(url: url!)
        //request.setValue("Basic d2ViYXBwOmlBTXdvcnRoeQ==", forHTTPHeaderField: "Authorization")
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchUsageFromElastic: %@", error!.localizedDescription)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    //       let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.usageData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    //         var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        let hitsParent=self.usageData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        
                        let hit = hits[0] as! NSDictionary
                        //let hit=hits[0] as! NSDictionary
                        let hit2=hit["_source"] as! NSDictionary
                        let usage=hit2["usage"] as! NSArray
                        
                        self.usageDataArray = usage as! [NSDictionary]
                        self.initUsageChart()
                        
                        
                        
                    })
                    
                }
            }
        })
        task.resume()
    }
    
    
    
    
    
    

    func refreshData() {
     //   solarChart.data=nil

        
        fetchUsageFromElastic()
    }
    
    
    
    
}

