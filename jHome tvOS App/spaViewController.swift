//
//  spaViewController.swift
//  jHome
//
//  Created by jeff guilfoyle on 5/26/17.
//  Copyright © 2017 jeff guilfoyle. All rights reserved.
//

import UIKit

class spaViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchSpaStatus()
    //   @objc  _ = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.fetchSpaStatus), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var phOrpLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var setTempLabel: UILabel!

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    var spaData: NSDictionary?
    ///spa/api/v1.0/status
    func fetchSpaStatus()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://spa2.local:5000/spa/api/v1.0/status"
        
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("%@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        let spaJsonData = jsonData as? NSDictionary
                        let spaStatus = spaJsonData?["status"] as? NSDictionary
                        self.spaData = spaStatus
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        //let hitsParent=self.spaData!["hits"]! as! NSDictionary
                        //let hits=hitsParent["hits"] as! NSArray
                        //let hit = hits[0] as! NSDictionary
                        //let hit2=hit["_source"] as! NSDictionary
                        //let spaTemp=hit2["temperature"] as! NSNumber
                        // let setTemp=hit2["settemp"] as! NSNumber
                        let spaTemp = self.spaData?["probetemp"] as! String
                        let spaTempNumber = Double(spaTemp)
                        let spaTempString = String(format: "%.1f", arguments: [spaTempNumber!])
                        self.currentTempLabel.text = "\(spaTempString)°F"
                        
                        if let spaSetTempString = self.spaData?["set_temperature"] as? String {
                            self.setTempLabel.text = "Set to \(spaSetTempString)°F"
                        }
                        
                        let spaPh = self.spaData?["ph"] as! String
                        let spaOrp = self.spaData?["orp"] as! String
                        self.phOrpLabel.text = "pH: \(spaPh) \nORP: \(spaOrp)"
                        
                    })
                }
            }
        })
        task.resume()
    }


}
