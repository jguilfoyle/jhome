//
//  ViewController.swift
//  jHome tvOS App
//
//  Created by jeff guilfoyle on 3/20/16.
//  Copyright © 2016 jeff guilfoyle. All rights reserved.
//


import UIKit
import Charts


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate {
    
    @IBOutlet weak var solarLabel: UILabel!
    
    @IBOutlet weak var demandLabel: UILabel!
    @IBAction func refreshButtonPress(_ sender: AnyObject) {
       // self.fetchFromMeter("get_instantaneous_demand")
        self.fetchGenerationFromElastic(1)
      //  self.fetchFromMeter("get_current_summation")
    }
    
   @IBOutlet weak var solarChart: LineChartView!
    
   @IBOutlet weak var usageChart: PieChartView!
    
    @IBOutlet weak var poolTemp: UILabel!
    
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet weak var windsLabel: UILabel!
    
    @IBOutlet weak var airTempLabel: UILabel!
    @IBOutlet weak var wuConditionsImage: UIImageView!
    @IBOutlet weak var sunImage: UIImageView!
    
    @IBOutlet weak var forecastLabel: UILabel!
    @IBOutlet weak var spaTempLabel: UILabel!
    
    @IBOutlet weak var tabBar: UITabBarItem!
    @IBOutlet weak var dailySolarLabel: UILabel!
    
    
    
    //let host="http://104.188.20.32:8200"
    //let host="http://192.168.1.129:8200"
    let host="http://34.217.173.42:9200"
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        //kill timers
        
        
        //kill chart data
        solarChart.data=nil
        self.solarDataPoints = []
        self.solarDataIndex = []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        solarChart.backgroundColor = nil
        usageChart.backgroundColor = nil

        if self.colors.count < 1 {
            self.setChartColors()
            
        }
        self.fetchCurrentMeterReading()
        self.getTodaysIndex()
        
       // self.fetchFromMeter()
        
      solarChart.data=nil
         self.solarDataPoints = []
        self.solarDataIndex = []
        self.fetchGenerationFromElastic(3000)
        
        fetchUsageFromElastic()
        fetchPoolTempFromElastic()
        fetchSpaStatus()
        //fetchSpaTempFromElastic()
        fetchWeatherUnderground()
        fetchWeatherUndergroundForecast()
        fetchGenerationFromSolarEdge()
        
    }
    
    func setChartColors() {
        
        
        let chartColors = [
            [0.000, 0.635, 1.000],
            
            [0.380, 0.847, 0.212],
            [0.973, 0.733, 0.008],
            [1.000, 0.149, 0.000],
            [0.761, 0.282, 0.522],
            [0.373, 0.373, 0.373]
            ]
        
        
        for i in 0..<4 {
            let alpha = 1.0 - Double(i)/4.0
            for colorTrio in chartColors {
                let color = UIColor(red: CGFloat(colorTrio[0]), green: CGFloat(colorTrio[1]), blue: CGFloat(colorTrio[2]), alpha: CGFloat(alpha))
                self.colors.append(color)
            }
        }
    }
    
    var colors: [UIColor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
   //     self.fetchFromMeter()
        
        //       self.fetchGenerationFromElastic(2000)
        
       _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.fetchCurrentMeterReading), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(ViewController.fetchCurrentSolar), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.fetchUsageFromElastic), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(ViewController.fetchPoolTempFromElastic), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(ViewController.fetchWeatherUnderground), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 1800, target: self, selector: #selector(ViewController.fetchWeatherUndergroundForecast), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(ViewController.fetchSpaStatus), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 900, target: self, selector: #selector(ViewController.fetchGenerationFromSolarEdge), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    var demandData: NSDictionary?
    
    

    
    
    func fetchFromMeter()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "https://api.rainforestcloud.com/rest/data/metering/demand/latest/d8d5b9009274"
        let url=URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        request.setValue("Bearer ygY68q9foSPfvlpdaS5n+zOkDsgIpJJbMeO3hcjj5lg=", forHTTPHeaderField: "Authorization")
        //request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
        //request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
        
        //let paramString = "<Command>\n<Name>\(command)</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
        //request.httpBody = paramString.data(using: String.Encoding.utf8)
        
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchFromMeter: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    if (dataString?.contains("Error") == true) {
                        NSLog("fetchFromMeter \(dataString!)")
                        self.demandLabel.text = "error"
                        self.demandData = nil
                    }
                    else {
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                            let meterData = jsonData as? [Any] //as? NSDictionary
                            let meterDataDict = meterData![0] as? [String: Any]
                            let meterEntries = meterDataDict!["entries"] as? [String: Any]
                           // for meterEntry in meterEntries! {
                            
                            for (key,value) in meterEntries! {
                                    print("\(key): \(value)")
                                let demandLabel = String(describing: value)
                                 DispatchQueue.main.async(execute: {
                                    self.demandLabel.text = "\(demandLabel) kWh"
                                })
                                
                              //  }
                            }
                            
                            
                            //self.demandData = jsonData as? NSDictionary
                            //   self.tableView.reloadData()
                        }
                        catch _ {
                            NSLog("error parsing JSON data")
                        }
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    
                    DispatchQueue.main.async(execute: {
                        if (self.demandData != nil) {
                            if (self.demandData!["InstantaneousDemand"] != nil) {
                                
                                let instantaneousDemand=self.demandData!["InstantaneousDemand"] as? NSDictionary
                                let currentDemandString=instantaneousDemand?["Demand"] as? String
                                
                                
                                
                           //     let currentDemandString=self.demandData!["InstantaneousDemand"]!["Demand"]! as? String
                                let parsedDemandString = currentDemandString?.replacingOccurrences(of: "0x", with: "")
                                //currentDemandString="0002c3"
                                var currentDemand = Int(parsedDemandString!, radix: 16)
                                if (currentDemand > 2147483647) {
                                    currentDemand = currentDemand! - 4294967296
                                }
                                let currentDemandkW = (Float(currentDemand!) / 1000.0)
                                let currentDemandOutput = "\(currentDemandkW) kW"
                                //       NSLog("current demand \(currentDemandkW) kW")
                                
                                self.demandLabel.text = currentDemandOutput
                            }
                            
                        }
                    })
                    
                }
            }
        })  
        task.resume()
    }
    
    
    
    
    
    
    
    
    
    
    var solarEdgeData: NSDictionary?
    
    
    @objc func fetchGenerationFromSolarEdge()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "https://monitoringapi.solaredge.com/site/231801/overview?api_key=3HPD58OS5IMPJUSP0AVRIAW29ZEUY19H&format=application/json"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchGenerationFromSolarEdge: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    //  let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.solarEdgeData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    
                    DispatchQueue.main.async(execute: {
                        let overview=self.solarEdgeData!["overview"] as! NSDictionary
                        let lastDayData=overview["lastDayData"] as! NSDictionary
                        let dailyTotal=(lastDayData["energy"] as! Double)/1000
                        
                       // let dailyTotal=(self.solarEdgeData!["overview"]!["lastDayData"]!!["energy"]! as? Double)!/1000
                        
                        
                        
                     //   self.sunImage.alpha = CGFloat((Double(currentPower!)/10.0)+0.2)
                        
                        self.dailySolarLabel.text = String(format: "%.2f kWh", dailyTotal)

                        
                    //    self.solarLabel.text = String(currentPower!)
                        
                    })
                    
                }
            }
        })  
        task.resume()
    }
    
    
    var solarDataPoints = [Double]()
    var solarDataIndex = [String]()
    

    func setSolarChart() {
        solarChart.backgroundColor = nil
        solarChart.chartDescription?.text = ""
        solarChart.noDataText = "You need to provide data for the chart."
        solarChart.legend.enabled = false
        solarChart.rightAxis.enabled = false
        
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<solarDataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: solarDataPoints[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = LineChartDataSet(values: dataEntries, label: "kW")
        
        
        let chartData=LineChartData(dataSet: chartDataSet)
        
        //let chartData = LineChartData(xVals: solarDataIndex, dataSet: chartDataSet)
        chartDataSet.lineWidth=2
        chartDataSet.circleRadius=0
        chartDataSet.colors=[UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        //chartDataSet.
        solarChart.leftAxis.axisMinimum = -0.001
        //solarChart.rightAxis.
        solarChart.data = chartData
 
    }
    
    
    
    var usageDataPoints = [Double]()
    var usageDataIndex = [String]()
    
    
    func initUsageChart() {
        usageChart.isOpaque = false
        usageChart.holeColor = nil
        usageChart.backgroundColor = nil
        usageChart.legend.enabled = false
        usageChart.chartDescription?.text = ""
       // usageChart.legend = ""
        //usageChart.t
        usageChart.noDataText = "You need to provide data for the chart."
        let circuitList = ["Lights","Lights2","Kitchen","A","Spa-A",
                           "Spa","AC-Theater-A","AC-Theater","Garage","Spare",
                           "Furnace","Master Bath","Pool-A","Pool","Fan",
                           "Wiring closet","Washer","Pool Bath","Play lights","Play outlets",
                           "F","Garage/Stair lights","MBR Lights","MBR Outlets", "1",
                           "2", "3", "4", "5", "6",
                           "30","31","AC"]
        var circuits: [String] = []
        
        var dataEntries: [ChartDataEntry] = []
        //  var dataIndex: [String] = []
        var index=0
        var circuit=0
        for usageItem  in self.usageDataArray! {
            var watts = usageItem["watts"] as! Double
            //     let circuit = usageItem["circuit"]
            //add pool circuits together
            if (circuit == 13 || circuit == 5 || circuit == 7)  {
                //circuit=circuit+1
                let secondCircuit = self.usageDataArray![circuit-1] 
                let watts2 = secondCircuit["watts"] as! Double
                
                watts = watts + watts2
                
            }
            
            
            if (circuit != 12 && circuit != 4 && circuit != 6 && watts > 0) {
                
                
                let dataEntry = PieChartDataEntry(value: watts, label: circuitList[circuit])
              //  x: watts, y: Double(index))
                circuits.append(circuitList[circuit])
                index=index+1
                dataEntries.append(dataEntry)
            }
            circuit=circuit+1
        }
        
        
        
        let chartDataSet = PieChartDataSet(values: dataEntries, label: "watts")
        chartDataSet.colors = self.colors
        
        
        let chartData = PieChartData(dataSet: chartDataSet)
        
      //  let chartData = PieChartData(-k w: circuits, dataSet: chartDataSet)
        
        
        //  chartDataSet.lineWidth=2
        //  chartDataSet.circleRadius=0
        //  chartDataSet.colors=[UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        //chartDataSet.
        //soChart.leftAxis.startAtZeroEnabled = true
        //solarChart.rightAxis.
       
        usageChart.data = chartData
        
    }
    
    
    
    
    
    @objc func fetchCurrentSolar() {
        self.fetchGenerationFromElastic(1)
        
    }
    
    var solarData: NSDictionary?

    
    func fetchGenerationFromElastic(_ count: Int)
    {
        if (count > 1) {
            solarChart.data=nil
            self.solarDataPoints = []
            self.solarDataIndex = []
        }
        let config = URLSessionConfiguration.ephemeral
        //    let urlString: String = "http://104.188.20.32:8200/solar/_search?sort=timestamp:desc&size=\(count)"
        let indexDate = self.getTodaysIndex()
        let urlString: String = "\(host)/solar-\(indexDate)/_search?sort=timestamp:desc&size=\(count)"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        //request.setValue("Basic d2ViYXBwOmlBTXdvcnRoeQ==", forHTTPHeaderField: "Authorization")
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchgenerationfromelastic: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    //     let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.solarData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        if let hitsParent=self.solarData?["hits"] as? NSDictionary {
                        let hits=hitsParent["hits"] as! [NSDictionary]
                        
                        for hit in hits.reversed() {
                            //let hit=hits[0] as! NSDictionary
                            let hit2=hit["_source"] as! NSDictionary
                            let generation=hit2["generation"] as! NSDictionary
                            let currentPower=generation["kva"] as! Double
                            // let kw
                            
                            
                            //   let currentPower = self.solarData!["hits"]!["generation"]!!["kva"]! as? Double
                            kw = currentPower/1000
                            self.sunImage.alpha = CGFloat(kw/9.0)
                            //       NSLog("current power \(kw) kW")
                            self.solarDataPoints.append(kw)
                            
                            self.solarDataIndex.append("")
                           
                        }
                        self.solarDataPoints.remove(at: 0)
                        self.solarDataIndex.remove(at: 0)
                        self.setSolarChart()
                        // solarChart.data = self.solarDataPoints
                        self.solarLabel.text = String("\(kw) kW")
                        
                        }
                    })
                    
                }
            }
        })  
        task.resume()
    }
    
    
    
    @objc func fetchCurrentMeterReading() {
        self.fetchMeterReadingFromElastic(1)
        
    }
    
     var meterData: NSDictionary?
    
    func fetchMeterReadingFromElastic(_ count: Int)
    {
        // http://34.217.173.42:9200/meter-*/_search?sort=timestamp:desc&size=1
        let config = URLSessionConfiguration.ephemeral
        let indexDate = self.getTodaysIndex()
        let urlString: String = "\(host)/meter-\(indexDate)/_search?sort=timestamp:desc&size=\(count)"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)

        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchgenerationfromelastic: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    //     let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                       self.meterData = jsonData as? NSDictionary
                        
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        if let hitsParent=self.meterData?["hits"] as? NSDictionary {
                            let hits=hitsParent["hits"] as! [NSDictionary]
                            
                            for hit in hits.reversed() {
                                //let hit=hits[0] as! NSDictionary
                                let hit2=hit["_source"] as! NSDictionary
                                let reading=hit2["reading"] as! NSDictionary
                                let currentReading=reading["kva"] as! Double
                                // let kw
                                
                                
                                //   let currentPower = self.solarData!["hits"]!["generation"]!!["kva"]! as? Double
                                kw = currentReading
                                //self.sunImage.alpha = CGFloat(kw/9.0)
                                //       NSLog("current power \(kw) kW")
                                //self.solarDataPoints.append(kw)
                                
                                //self.solarDataIndex.append("")
                                
                            }
                            //self.solarDataPoints.remove(at: 0)
                            //self.solarDataIndex.remove(at: 0)
                            //self.setSolarChart()
                            // solarChart.data = self.solarDataPoints
                            self.demandLabel.text = String("\(kw) kW")
                            
                        }
                    })
                    
                }
            }
        })
        task.resume()
    }
    
    
    
    var usageData: NSDictionary?
    var usageDataArray: [NSDictionary]?
    
    // get Ted Spyder usage per circuit
    // http://192.168.1.129:9200/usage/_search?sort=timestamp:desc&size=1
    @objc func fetchUsageFromElastic()
    {
        
        let config = URLSessionConfiguration.ephemeral
        //    let urlString: String = "http://104.188.20.32:8200/solar/_search?sort=timestamp:desc&size=\(count)"
        let indexDate = self.getTodaysIndex()
        let urlString: String = "\(host)/usage-\(indexDate)/_search?sort=timestamp:desc&size=1"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        //request.setValue("Basic d2ViYXBwOmlBTXdvcnRoeQ==", forHTTPHeaderField: "Authorization")
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchUsageFromElastic: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    //       let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.usageData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    //         var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        let hitsParent=self.usageData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        
                        let hit = hits[0] as! NSDictionary
                        //let hit=hits[0] as! NSDictionary
                        let hit2=hit["_source"] as! NSDictionary
                        let usage=hit2["usage"] as! NSArray
                        
                        self.usageDataArray = usage as? [NSDictionary]
                        self.initUsageChart()
                        
                        
                        
                    })
                    
                }
            }
        })  
        task.resume()
    }
    
    
    
    
    
    
    var poolData: NSDictionary?
    
    
    @objc func fetchPoolTempFromElastic()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let indexDate = self.getTodaysIndex()
        let urlString: String = "\(host)/pool-\(indexDate)/_search?sort=timestamp:desc&size=1"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchPoolTempFromElastic: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.poolData = jsonData as? NSDictionary
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        let hitsParent=self.poolData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        let hit = hits[0] as! NSDictionary
                        let hit2=hit["_source"] as! NSDictionary
                        let poolTemp=hit2["pooltemp"] as! NSNumber
                        //let setTemp=hit2["settemp"] as! NSNumber
                        
                        self.poolTemp.text = "\(poolTemp)"
                    })
                }
            }
        })  
        task.resume()
    }
    
    var spaData: NSDictionary?
  ///spa/api/v1.0/status
    @objc func fetchSpaStatus()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://power.local:5000/spa/api/v1.0/status"

        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchSpaStatus: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        let spaJsonData = jsonData as? NSDictionary
                        let spaStatus = spaJsonData?["status"] as? NSDictionary
                        self.spaData = spaStatus
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        //let hitsParent=self.spaData!["hits"]! as! NSDictionary
                        //let hits=hitsParent["hits"] as! NSArray
                        //let hit = hits[0] as! NSDictionary
                        //let hit2=hit["_source"] as! NSDictionary
                        //let spaTemp=hit2["temperature"] as! NSNumber
                        // let setTemp=hit2["settemp"] as! NSNumber
                        let spaTemp = self.spaData?["temperature"] as! String
                        let spaTempNumber = Double(spaTemp)
                        let spaTempString = String(format: "%.0f", arguments: [spaTempNumber!])
                        self.spaTempLabel.text = "\(spaTempString)"
                    })
                }
            }
        })
        task.resume()
    }

    
    
    func fetchSpaTempFromElastic()
    {
        
        let config = URLSessionConfiguration.ephemeral
        let indexDate = self.getTodaysIndex()
        let urlString: String = "\(host)/spa-\(indexDate)/_search?sort=timestamp:desc&size=1"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchSpaTempFromElastic: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.spaData = jsonData as? NSDictionary
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        let hitsParent=self.spaData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        let hit = hits[0] as! NSDictionary
                        let hit2=hit["_source"] as! NSDictionary
                        let spaTemp=hit2["temperature"] as! NSNumber
                       // let setTemp=hit2["settemp"] as! NSNumber
                        
                        self.spaTempLabel.text = "\(spaTemp)"
                    })
                }
            }
        })  
        task.resume()
    }
    
    
    // wunderground key
    //   efd234ca5e26b934
    
    
    
    var weatherData: NSDictionary?
    
    @objc func fetchWeatherUnderground()
    {
        NSLog("fetching weather")
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://api.wunderground.com/api/efd234ca5e26b934/conditions/q/92117.json"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchWeatherUnderground: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                        self.weatherData = jsonData["current_observation"] as? NSDictionary
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        if let temp = self.weatherData?["temperature_string"] as? String {
                            self.airTempLabel.text = "\(temp)"
                            self.conditionsLabel.text = self.weatherData!["weather"] as? String
                            self.windsLabel.text = self.weatherData!["wind_string"] as? String
                        }
                    })
                }
            }
        })  
        task.resume()
    }
    
    var forecastData: NSArray?
    
    @objc func fetchWeatherUndergroundForecast()
    {
        NSLog("fetching weather")
        let config = URLSessionConfiguration.ephemeral
        let urlString: String = "http://api.wunderground.com/api/efd234ca5e26b934/forecast/q/92117.json"
        let url=URL(string: urlString)
        let request = URLRequest(url: url!)
        
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchWeatherUndergroundForecast: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
                        
                        let forecast=jsonData?["forecast"] as? NSDictionary
                        let txt_forecast=forecast?["txt_forecast"] as! NSDictionary
                        self.forecastData=txt_forecast["forecastday"] as? NSArray
                        
                       // self.forecastData = jsonData["forecast"]!!["txt_forecast"]!!["forecastday"] as? NSArray
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    DispatchQueue.main.async(execute: {
                        let forecastDataFirst=self.forecastData?[0] as? NSDictionary
                        if let forecasttext = forecastDataFirst?["fcttext"] as? String {
                            self.forecastLabel.text = forecasttext
                        }
                        
                    })
                }
            }
        })  
        task.resume()
    }
    
    
    func fetchImage(_ url: String)
    {
        
        let config = URLSessionConfiguration.ephemeral
        
        let url=URL(string: url)
        var request = URLRequest(url: url!)
        request.setValue("Basic d2ViYXBwOmlBTXdvcnRoeQ==", forHTTPHeaderField: "Authorization")
        /*
         request.HTTPMethod = "POST"
         request.setValue("002FA2", forHTTPHeaderField: "Cloud-ID")
         request.setValue("jeff@guilfoyle.net", forHTTPHeaderField: "User")
         request.setValue("PbeD7dNI", forHTTPHeaderField: "Password")
         
         let paramString = "<Command>\n<Name>get_instantaneous_demand</Name>\n<MacId>0xd8d5b90000006783</MacId>\n<Format>JSON</Format>\n</Command>\n"
         request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
         */
        let session = URLSession(configuration: config, delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                NSLog("fetchImage: %@", error!.localizedDescription)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    //             let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                        self.poolData = jsonData as? NSDictionary
                        //   self.tableView.reloadData()
                    }
                    catch _ {
                        NSLog("error parsing JSON data")
                    }
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    //          var kw: Double = 0
                    DispatchQueue.main.async(execute: {
                        
                        let hitsParent=self.poolData!["hits"]! as! NSDictionary
                        let hits=hitsParent["hits"] as! NSArray
                        
                        let hit = hits[0] as? NSDictionary
                        //let hit=hits[0] as! NSDictionary
                        let hit2=hit?["_source"] as! NSDictionary
                        let poolTemp=hit2["pooltemp"] as! NSNumber
                        //let setTemp=hit2["settemp"] as! NSNumber
                        
                        self.poolTemp.text = "\(poolTemp)"
                        
                        
                    })
                    
                }
            }
        })  
        task.resume()
    }
    
    
    func hideTabBar(){
       // self.tabBar.
    }
    
    func getTodaysIndex() -> String {
        let date = NSDate()
        // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        //let defaultTimeZoneStr = formatter.string(from: date as Date)
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        let utcTimeZoneStr = formatter.string(from: date as Date)
        // "2014-07-23 18:01:41 +0000" <-- same date, now in UTC
       // NSLog("\(utcTimeZoneStr)")
        return utcTimeZoneStr
    }
    
    
    
    @objc func refreshData() {
        solarChart.data=nil
        self.solarDataPoints = []
        self.solarDataIndex = []
        self.fetchGenerationFromElastic(3000)
        
        fetchUsageFromElastic()
        fetchPoolTempFromElastic()
        fetchSpaStatus()
        //fetchSpaTempFromElastic()
        fetchWeatherUnderground()
        fetchWeatherUndergroundForecast()
        fetchGenerationFromSolarEdge()
    }
    
    
    
    
}
